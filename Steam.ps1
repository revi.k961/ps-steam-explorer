while ($true) {
    $steamProcesses = Get-Process -Name "steam" -ErrorAction SilentlyContinue

    if ($steamProcesses) {
        Stop-Process -Name "explorer" -Force
    } else {
        $explorerProcesses = Get-Process -Name "explorer" -ErrorAction SilentlyContinue
        if (-not $explorerProcesses) {
            Start-Process -FilePath "C:\Windows\explorer.exe"
        }
    }

    Start-Sleep -Seconds 1
}