# Powershell Script that close Explorer or Started when steam closed

A Quick Code Insight:

This code uses the Get-Process cmdlet to check if the Steam process is running. If it is, the code stops the Explorer process. If the Steam process is not running, the code checks if the Explorer process is running. If it is not, the code starts the Explorer process. The code then sleeps for one second and repeats the process.

Perfect for those who have Steam Big Picture as Winlogon Shell.

If you want to start the script without Powershell, the .ps1 script must be compiled to an application.

PS. You have create a new Task in Task Scheduler